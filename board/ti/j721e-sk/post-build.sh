#!/bin/sh
BOARD_DIR="$(dirname $0)"

# u-boot is looking at /boot/dtb/ti for devicetree in the rootfs
# partition while Buildroot install the kernel and its devicetree
# in /boot (BR2_LINUX_KERNEL_INSTALL_TARGET enabled).
# Enabling BR2_LINUX_KERNEL_DTB_KEEP_DIRNAME allows to keep the
# "ti" subdirectory but "dts" is still missing.
# Make sure /boot/dtb/ti exist by creating a symlink in
# ${TARGET_DIR}/boot
# https://git.yoctoproject.org/meta-ti/tree/meta-ti-bsp/recipes-kernel/linux/ti-kernel.inc?h=09.02.00.004#n13
# https://source.denx.de/u-boot/u-boot/-/blob/v2024.01/include/env/ti/mmc.env?ref_type=tags#L18
if [ -L ${TARGET_DIR}/boot/dtb ]; then
    rm ${TARGET_DIR}/boot/dtb
    ln -sf . ${TARGET_DIR}/boot/dtb
fi
