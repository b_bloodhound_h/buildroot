################################################################################
#
# ti-k3-boot-firmware
#
################################################################################

TI_K3_BOOT_FIRMWARE_VERSION = 09.01.00.006
TI_K3_BOOT_FIRMWARE_SITE = https://git.ti.com/cgit/processor-firmware/ti-linux-firmware/snapshot
TI_K3_BOOT_FIRMWARE_SOURCE = ti-linux-firmware-$(TI_K3_BOOT_FIRMWARE_VERSION).tar.xz
TI_K3_BOOT_FIRMWARE_INSTALL_IMAGES = YES
TI_K3_BOOT_FIRMWARE_LICENSE = TI Proprietary
TI_K3_BOOT_FIRMWARE_LICENSE_FILES = LICENSE.ti

ifneq ($(BR2_TARGET_TI_K3_BOOT_FIRMWARE_DM),)
define TI_K3_BOOT_FIRMWARE_INSTALL_DM_FILES
	mkdir -p $(BINARIES_DIR)/ti-dm
	cp -dpfr $(@D)/ti-dm/$(BR2_TARGET_TI_K3_BOOT_FIRMWARE_DM) $(BINARIES_DIR)/ti-dm
endef
endif

define TI_K3_BOOT_FIRMWARE_INSTALL_IMAGES_CMDS
	mkdir -p $(BINARIES_DIR)/ti-sysfw
	cp -dpfr $(@D)/ti-sysfw/*$(BR2_TARGET_TI_K3_BOOT_FIRMWARE_SOC)* $(BINARIES_DIR)/ti-sysfw
	$(TI_K3_BOOT_FIRMWARE_INSTALL_DM_FILES)
endef

$(eval $(generic-package))
