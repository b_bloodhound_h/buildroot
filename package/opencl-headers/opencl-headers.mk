################################################################################
#
# opencl-headers
#
################################################################################

OPENCL_HEADERS_VERSION = 2023.04.17
OPENCL_HEADERS_SOURCE = OpenCL-Headers-$(OPENCL_HEADERS_VERSION).tar.gz
OPENCL_HEADERS_SITE = $(call github,KhronosGroup,OpenCL-Headers,v$(OPENCL_HEADERS_VERSION))
OPENCL_HEADERS_LICENSE = Apache-2.0
OPENCL_HEADERS_LICENSE_FILES = LICENSE
OPENCL_HEADERS_INSTALL_STAGING = YES

OPENCL_HEADERS_INSTALL_TARGET = NO

OPENCL_HEADERS_FILES = cl_d3d10.h \
					   cl_d3d11.h \
					   cl_dx9_media_sharing.h \
					   cl_dx9_media_sharing_intel.h \
					   cl_egl.h \
					   cl_ext.h \
					   cl_ext_intel.h \
					   cl_gl_ext.h \
					   cl_gl.h \
					   cl.h \
					   cl_half.h \
					   cl_icd.h \
					   cl_layer.h \
					   cl_platform.h \
					   cl_va_api_media_sharing_intel.h \
					   cl_version.h \
					   opencl.h


define OPENCL_HEADERS_INSTALL_STAGING_CMDS
	$(foreach header,$(OPENCL_HEADERS_FILES), \
		$(INSTALL) -D -m 0644 $(@D)/CL/$(header) \
			$(STAGING_DIR)/usr/include/CL/$(header)
	)
endef

$(eval $(generic-package))
