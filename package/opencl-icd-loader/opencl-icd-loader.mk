################################################################################
#
# opencl-icd-loader
#
################################################################################

OPENCL_ICD_LOADER_VERSION = 2023.04.17
OPENCL_ICD_LOADER_SOURCE = OpenCL-ICD-Loader-$(OPENCL_ICD_LOADER_VERSION).tar.gz
OPENCL_ICD_LOADER_SITE = $(call github,KhronosGroup,OpenCL-ICD-Loader,v$(OPENCL_ICD_LOADER_VERSION))
OPENCL_ICD_LOADER_LICENSE = Apache-2.0
OPENCL_ICD_LOADER_LICENSE_FILES = LICENSE
OPENCL_ICD_LOADER_INSTALL_STAGING = YES

OPENCL_ICD_LOADER_DEPENDENCIES = opencl-headers

OPENCL_ICD_LOADER_CONF_OPTS += -DOPENCL_ICD_LOADER_HEADERS_DIR=$(STAGING_DIR)/usr/include

$(eval $(cmake-package))
$(eval $(host-cmake-package))
